<?php

//Bucle while
$i = 1;

while ($i <= 10) 
{
    echo "Hola Mundo $i <br>";
    $i++;
}

$b = 8;

//Bucle do-while, por lo menos una única vez se ejecute la sentencia
do {
    echo "Hola Mundo $b <br>";
    $i++;
} while ($i <= 10);

echo "Has salido del bucle <br>";

$base = 2;
$exp = 5;

$init = 1;
$resultado = 1;

//Ejercicio versión mía
while ( $init <= $exp)
{
    $resultado = $resultado * $base;
    $init++;
}

//echo "El resultado de la potencia es $result";
echo "El valor de $base elevado a la $exp es: $resultado";

?>
