<?php
$base = 5;
$exp = 4;

$resultado = 1;

for ($i=0; $i < $base; $i++) 
{
    $resultado = $resultado * $base;
}

echo "El valor de $base elevado a la $exp es: $resultado <br>";

//Ejercicio: Imprimir arbol de asteriscos

$filas = 10;

for ($j=0; $j < $filas; $j++)
{
    for ($k=0; $k <= $j; $k++)
    {
        echo '*';
    }
    echo "<br>";
}

echo "<br>";

$nombres = ['Jose', 'Iris', 'Elizabeth', 'Pedro'];

//echo $nombres[1];

foreach($nombres as $nombre)
{
    echo $nombre ."<br>";
}

$otros_nombres = ['Andres', 'Joanna', 'Eliana', 'Pablo'];

foreach($otros_nombres as $indice => $nombre)
{
    echo $nombre ." se encontaraba en el índice $indice <br>";
}

?>