<?php

for ($i=1; $i <= 20 ; $i++)
{
    if ($i == 11)
    {
        break;
    }

    echo "$i <br>";
}

echo "Saliste del bucle con break <br>";

for ($i=1; $i <= 20 ; $i++)
{
    if ($i == 7 || $i == 12)
    {
        continue;
        //Continúa a la siguiene iteración y no imprime ni 7 ni 12 en pantalla
    }
    echo "$i <br>";
}

for ($i=1; $i <= 20 ; $i++)
{
    if ($i == 7 || $i == 12)
    {
        exit;
        //die;
        //Las sentencias "die" o "exit" culminan la ejecución del bucle
    }
    echo "$i <br>";
}

echo "Esta línea no se ejecutará por la instrucción exit";

?>