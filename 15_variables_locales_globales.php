<?php
$a = 5;
$b = 10;

function test()
{
	//$a = 3;
	/*Aunque lleven el mismo nombre, para PHP son variables distintas
	  Usaremos la palabra reservada "global" para acceder a la variable global*/
	  //global $a = 3;
	  global $a;
	  //$a = 10;
	  
	  //echo $a;
}

//Parámetro por valor
function test2($n)
{
	$n = $n + 10;
}

//Parámetro por referencia
function test3(&$n)
{
	$n = $n + 10;
}

//test();
//echo "<br>";
//echo $a;

test2($a);
test2($b);
echo $a ."<br>";
echo $b ."<br>";
test3($a);
test3($b);
echo "<br>";
echo "Parámetros tomados por referencia <br>";
echo $a ."<br>";
echo $b ."<br>";
?>