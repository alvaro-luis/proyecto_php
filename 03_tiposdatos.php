<?php
$numero_entero = 123; //Integer
$numero_flotante = 123.45; //Decimal - dato de tipo flotante
$numero_doble = 123.45565071; //Decimal - dato de tipo doble

$cadena = "Hola mundo";
$cadena_con_comillas = 'Hola mundo mi nombre es: "Fulano"'; //Las comillas simples sirven para meter comillas dobles dentro de una cadena
$cadena_con_comillas_dobles = "Hola mundo mi nombre es: 'Fulano' y tengo \"20\" años"; //Comillas escapadas
$cadena_con_signo_dolar = "Hola mundo mi nombre \$es: 'Fulano' y tengo \"20\" años"; //Comillas escapadas

$bool = true; 


/*
No tenemos que especificar el tipo de dato que estamos almacenando, 
PHP lo dscubre automáticamente de acuerdo al valor asignado
*/

//echo "El numero decimal es: ".$numero_flotante;
echo $cadena_con_signo_dolar;
echo " <br> ";
echo $bool; //Imprime 1 si es true

/*
Cualquier número positivo o negativo se considera true, solo el 0 se considera false 
Toda cadena se considera por defecto como true, excepto si la cadena está vacía
*/

?>