<?php

$datos =[
 [
    'nombre'    => 'Fulano Fulanez',
	'email'     => 'fulano@fulanez.com',
	'celular'   => 4008002020,
	'direccion' => [
	    'pais'  => 'Perú',
		'ciudad'=> 'Lima' 
	]
 ],
 [
  'nombre'  => 'Pamela Pamelez',
  'email'   => 'pamela@pamelez.com',
  'celular' => 4008002020
 ],
 [
  'nombre'  => 'Joel Cordova',
  'email'   => 'joel@cordova.com',
  'celular' => 4008002020
 ]
];

echo $datos[0]['nombre'];
echo "<br>";
echo $datos[1]['nombre'];
echo "<br>";
echo $datos[0]['email'];
echo "<br>";
echo $datos[0]['direccion']['pais'];
echo "<br>";

echo "Imprimir todos el nombre de todos los usuarios (ciclo for): <br>";
echo "<br>";

//Obtener cantidad de elementos del array
$cantidad_elementos = count($datos);

 for ($j=0; $j < $cantidad_elementos; $j++)
 {
	 echo $datos[$j]['nombre'] ."<br>";
 }
echo "<br>";
 
echo "Imprimir todos el nombre de todos los usuarios (ciclo foreach): <br>";
echo "<br>";


 foreach($datos as $item)
 {
	 echo $item['nombre'] ."<br>";
	 echo $item['email'] ."<br>";
	 echo $item['celular'] ."<br>";
	 echo "<hr>"; //Líena de separación
 }

?>