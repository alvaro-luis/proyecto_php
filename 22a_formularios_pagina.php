<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>

    <form action="22b_procesar_formularios.php" method="POST" enctype="multipart/form-data">
        <label>
            Nombre:
            <input type="text" name="name">
        </label>

        <br>

        <label>
            Edad:
            <input type="number" name="edad">
        </label>

        <p>Sexo: </p>

        <select name="sexo">
            <option value="Masculino">Masculino</option>
            <option value="Femenino" selected>Femenino</option>
        </select>

        <!-- <label>
            Masculino:
            <input type="radio" name="sexo" value="Masculino">
        </label>

        <label>
            Femenino:
            <input type="radio" name="sexo" value="Femenino">
        </label> -->

        <br>

        <p>Roles:</p>

        <label>
            Administrador:
            <input type="checkbox" name="roles[]" value="Administrador">
        </label>

        <label>
            Editor:
            <input type="checkbox" name="roles[]" value="Editor">
        </label>

        <label>
            Moderador:
            <input type="checkbox" name="roles[]" value="Moderador">
        </label>

        <br>

        <label>
            Imagen:
            <br>
            <input type="file" name="image">
        </label>

        <br>
        <br>

        <label for="">Mensaje</label>
        <br>
        <textarea name="mensaje" cols="10" rows="6"></textarea>
        <br>
        <button type="submit">Enviar</button>
    </form>


</body>

</html>