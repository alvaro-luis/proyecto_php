<?php

$a = 5;
$b = 3;

echo "Primer párrafo <br>";

//Condicional simple
if ($a < $b)
{
    echo "Segundo párrafo <br>";
}

$c = 5;
$d = 3;

//Condicional compuesta
if ($c < $d)
{
    echo "Cuarto párrafo <br>";
}else
{
    echo "quinto párrafo <br>";
}

//Condicional Anidada
if ($c < $d)
{
    echo "Sexto párrafo <br>";
}else if ($c > $d)
{
    echo "Séptimo párrafo <br>";
}else
{
    echo "Octavo párrafo <br>";
}


?>