<?php

$cadena = "aeiou";
$cadena_con_tilde = "aéiou";

echo $cadena[1];
echo"<br>";
echo strlen($cadena);
echo"<br>";
echo strlen($cadena_con_tilde);//Cuando lleva tilde cuenta un bit más
echo"<br>";
echo mb_strlen($cadena_con_tilde);//Con mb_strlen se cuenta mejor la cantidad de caracteres
echo"<br>";
echo"Indice donde está el caracter en la cadena: <br>";
echo strpos($cadena,'i');
echo"<br>";

$cadena = "Hola mundo como estás";

echo"Corrobora si existe la palabra en la cadena: <br>";
echo str_contains($cadena,'mundo');
echo"<br>";

echo"Corrobora si existe la palabra en la cadena (Con operador ternario): <br>";
echo str_contains($cadena,'mundo') ? "Si se encuentra " : "No se encuentra";
echo"<br>";

echo"Corrobora si la cadena empieza con dicha palabra <br>";
echo str_starts_with($cadena,'Hola') ? "Si empieza con Hola" : "No empieza con Hola";
echo"<br>";

echo"Corrobora si la cadena finaliza con dicha palabra <br>";
echo str_ends_with($cadena,'estás') ? "Si finaliza con la palabra" : "No finaliza con la palabra";
echo"<br>";

$cadena1 = "Prueba";
$cadena2 = "prueba";

echo"Comparar si dos cadenas son iguales <br>";
if(strcmp($cadena1, $cadena2) == 0)
{
	echo "Las dos cadenas son iguales";
}else
{
	echo "Las cadenas son diferentes";
}
echo"<br>";

echo"Comparar si dos cadenas son iguales sin considerar mayúsculas o minúsculas <br>";
if(strcasecmp($cadena1, $cadena2) == 0)
{
	echo "Las dos cadenas son iguales";
}else
{
	echo "Las cadenas son diferentes";
}
echo"<br>";

$cadena = "Hola mundo";
echo "Colocar índice de a partir de qué palabra se comienza a mostrar la cadena en pantalla <br>";
echo substr($cadena, 1);
echo"<br>";

$cadena = "Hola mundo";
echo "Colocar índices de a partir de qué palabra se comienza a mostrar y hasta donde la cadena en pantalla <br>";
echo substr($cadena, 1, 5);
echo"<br>";

$cadena = "Hola mundo";
echo "Con número negativo hace lo mismo pero desde el final de la cadena <br>";
echo substr($cadena, -2);
echo"<br>";

$cadena = "Hola mundo";
echo "Poner el valor negativo primero para que haga el recorrido de atrá hacia delante, y luego poner positivo para que tome lo que queda en sentido inverso <br>";
echo substr($cadena, -5, 3);
echo"<br>";

$cadena = "Hola mundo";
echo "Cambiar el valor de una palabra por otra que se encuentra en una cadena <br>";
echo str_replace("mundo", "país", $cadena);
echo"<br>";

$cadena = "Hola mundo";
echo "Pasar cadena a minúscula <br>";
echo strtolower($cadena);
echo"<br>";

$cadena = "Hola mundo";
echo "Pasar cadena a mayúscula <br>";
echo strtoupper($cadena);
echo"<br>";

$cadena = "hola mundo";
echo "Imprimir solo la primera letra en mayúscula <br>";
echo ucfirst($cadena);
echo"<br>";

echo "Imprimir solo la primera letra en mayúscula de cada palabra <br>";
echo ucwords($cadena);
echo"<br>";



?>