<?php

//Operadores booleanos
$a = 10;
$b = 10;
$c = 5;

//Imprime 1 si es true, 0 si els false
//echo $a == $b;


echo "$a == $b: " .var_dump($a == $b);
echo " <br> ";
echo " $a == $c: "  .var_dump($a == $c);
echo " <br> ";
echo " $a <> $c: ".var_dump($a <> $c);
echo " <br> ";

//Operadores booleanos

$a = true;
$b = true;
$c = false;

$d = "Hola mundo";
$e = 0;

var_dump( $a && $b);
echo " <br> ";
var_dump( $a && $c);
echo " <br> ";
var_dump( $d && $e); //cadena y numeros comparados dan true
echo " <br> ";

//Cuando una variable tiene un valor nulo (null), cadena vacía o el número "0", en comparación darán nulo

$d = 4;
$e = 5;

var_dump( $d || $e);
echo " <br> ";
var_dump( $d < $e || $e > 3);

?>