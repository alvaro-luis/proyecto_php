<?php

$array =[10, 13, 15, 20];

//Imprimir elemento en la posición 3
echo $array[3];
echo "<br>";

echo "Agregar elemento al array(...) <br>";
	$array[] = 17; 

echo "Elemento agregado al array <br>";
echo $array[4];

//Otra manera de crear un array
$array[] = 10; 
$array[] = 13; 
$array[] = 15; 
$array[] = 20; 
$array[] = 17;
echo "<br>";

echo "A diferencia de otros lenguajes de programación, PHP nos permite almacenar distintos tipos de datos <br>";
$arrayDatosVariados =[10, "Fulano", 17, 20, false];
echo $arrayDatosVariados[1];
echo "<br>";

echo "Otra peculiaridad de PHP es que nos permite utilizar índices de tipo string dentro de nuestros arrays <br>";

$datos =[
    'nombre'  => 'Fulano Fulanez',
	'email'   => 'fulano@fulanez',
	'celular' => 4008002020
];

echo $datos['email'];
echo "<br>";
echo "Hacerlo de esta forma es muy útil cuando, por ejemplo, recuperamos información de nuestra base de datos que tiene cabeceras <br>";

?>