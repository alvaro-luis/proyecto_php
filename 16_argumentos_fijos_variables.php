<?php

function valoracion($nombre, $rating = 5) //valor por defecto
{
	echo "El $nombre tiene un rating de $rating";
}

//valoracion("Curso PHP 8 desde cero", 5);
//valoracion("Curso PHP 8 desde cero", 4);

function concatenar(...$palabras)
{
	$resultado = "";
	
	foreach($palabras as $palabra)
	{
		//$resultado = $resultado . $palabra. " ";
		$resultado .= $palabra. " ";
		//Variable de asignación combinada usándose con el concatenador
	}
	
	echo $resultado;
}

concatenar("Curso", "PHP", "8", "desde", "0");

?>