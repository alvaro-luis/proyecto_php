<?php

declare(strict_types = 1);

//function sumarEnteros(int $entero1, int $entero2)
function sumarEnteros(int $entero1, int $entero2): int //Especificar el tipo de dato que retornará de la función
{
	//return ($entero1 + $entero2) / 2; No da error de sintaxis, pero da error por el tipo de dato que definimos para retornar
	return ($entero1 + $entero2);
}

//$resultado = sumarEnteros(2, 5.4); Dará error debido al strict_types
$resultado = sumarEnteros(2, 5);

echo $resultado;

function sumarEnteros2(int $entero1, int $entero2): int | float//Especificar el tipo de dato que retornará de la función
{
	return ($entero1 + $entero2) / 2; //Ahora puede retornar int o float
}
echo"<br>";
$resultado = sumarEnteros2(2, 5);
echo $resultado;


?>