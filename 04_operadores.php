<?php
//Existen los operadores matemáticos y de asignación

$variable = 34; //El "=" es un operador de asignación y el "34" es el valor

$variable_negativa = -$variable;

//echo $variable_negativa;

$a = 2;
$b = 5;

$c1 = $a + $b;
$c2 = $a - $b;
$c3 = $a * $b;
$c4 = $b / $a;
$c5 = $b % $a; //operador modulo: Devuelve el residuo que existe entre dividir un número por otro
$c6 = 4 * 5 - 6 + 4 * 2 + 1 * 0; //Primero las multiplicaciones y divisines, luego van la sumas y restas
$c7 = 4 * (5 - 6) + 4 * 2 + 1 * 0; //Los paréntesis ayudan con la jerarquía de operaciones, aun sobre multiplicaciones y divisiones

echo $c1;
echo " <br> ";
echo $c2;
echo " <br> ";
echo $c3;
echo " <br> ";
echo $c4;
echo " <br> ";
echo $c5    ;
echo " <br> ";
echo $c6    ;
echo " <br> ";
echo $c7    ;

?>