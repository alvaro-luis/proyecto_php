<?php

//Estructuras algorítmica de selección múltiple

$a = 14;

switch ($a) {
        case 1:
        echo 'Lunes <br>';
        # code...
        break;
        case 2:
        echo 'Martes <br>';
        # code...
        break;
        case 3:
        echo 'Miercoles <br>';
        # code...
        break;
        case 4:
        echo 'Jueves <br>';
        # code...
        break;
        case 5:
        echo 'Viernes <br>';
        # code...
        break;
        case 6:
        echo 'Sábado <br>';
        # code...
        break;
        case 7:
        echo 'Domingo <br>';
        # code...
        break;

    default:
        echo 'El valor de la variable a no es un valor válido <br>';
        break;
}

echo match($a)
{
    1 => "Lunes",
    2 => "Martes",
    3 => "Miercoles",
    4 => "Jueves",
    5 => "Viernes",
    6 => "Sábado",
    7 => "Domingo",
    default => 'El valor de la variable a no es un valor válido'
};

?>