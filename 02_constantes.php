<?php 
define('CURSO', 'PHP 8 desde 0');
define('USUARIO_1', 'Fulano'); //Guion media para nombres compuestos

//A aprtir de PHP 7 se pueden definir cosntantes de tipo array
define('ANIMALES',[
    'perro',
    'gato',
    'loro',
    'tortuga',
]);
echo ANIMALES[0] ." ";

if (defined('CURSO')) {
    echo "Si se ha definido";
}

//Variables globales definidas por PHP
echo " Mi versión de PHP es: ". PHP_VERSION;
echo " <br> ";
echo " Mi sistema operativo es: ". PHP_OS;//Versión sistema operativo
echo " <br> ";
echo PHP_EXTENSION_DIR;
echo " <br> ";
echo PHP_SAPI;//Api del servidor
echo " <br> ";
echo __LINE__;//En qué línea se ejecuta esto
echo " <br> ";
echo __FILE__;//Retorna el directorio completo en el cual me encuentro

?>