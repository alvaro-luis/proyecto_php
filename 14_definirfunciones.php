<?php
//Factorial
$resultado = 1;
for ($i=5; $i > 0 ; $i--)
{
    $resultado = $resultado * $i;
}

//echo $resultado;

function factorial()
{
    $numero = 9;
    $resultado = 1;

for ($i=1; $i <= $numero; $i++)
{
    $resultado = $resultado * $i;
}

echo "El factorial de $numero es: " .$resultado;
}

//Funcion con parámetro

function factorial_con_parametro($numero)
{
    $resultado = 1;

for ($i=1; $i <= $numero; $i++)
{
    $resultado = $resultado * $i;
}

echo "El factorial de $numero es: " .$resultado;
}

//Funcion con parámetro y retorno

function factorial_con_parametro_retorno($numero)
{
    $resultado = 1;
    for ($i=1; $i <= $numero; $i++)
    {
    $resultado = $resultado * $i;
}

return $resultado;
//echo "El factorial de $numero es: " .$resultado;
}

factorial();
echo "<br>";
factorial_con_parametro(6);
echo "<br>";
$result = factorial_con_parametro_retorno(4);
echo "El factorial es: " .$result;
?>