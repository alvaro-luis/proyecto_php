<?php

$array = [1, 2, 3];

// $a =$array[0];
// $b =$array[1];
// $c =$array[2];

/* Esta función nos permite declarar variables a las que se les asignará
    un v  */
list($a, $b, $c) = $array;

echo $b;
echo "<br>";

$array_con_rango = range(10, 20);

var_dump($array_con_rango);
echo "<br>";

echo $array_con_rango[5];
echo "<br>";

echo "Cantidad de elementos que tiene un array: <br>";
echo count($array_con_rango);

echo "Verificar si existe un elemento dentro de un array: <br>";
echo "<br>";
$array_con_nombres = ['Victor', 'Pamela', 'Elizabeth', 'Iris'];

if(in_array('Elizabeth', $array_con_nombres))
{
	echo 'El valor buscado se encuentra dentro del array ';
}
else
{
	echo 'El valor buscado no se encuentra dentro del array';
}
echo "<br>";

echo "Borrar elemento de un array: <br>";
unset($array_con_nombres[2]);
var_dump($array_con_nombres);

?>