<?php

$a = 10;

$a++;

echo $a;

echo " <br> ";

$b = 10;

$b--;

echo $b;

echo " <br> ";

$c = 20;
//$d = $c++;
$d = ++$c;

echo "El valor de c es: $c <br>";
echo "El valor de d es: $d <br>";

$e = 10;
$f = 6;

//$e = $e + $f;
$e += $f; //Suma e $e mas $f, como en la instrucción inmediatamente de arriba

echo $e;

//aplica lo mismo para otros operadores aritméticos
$e -= $f;
$e *= $f;
$e /= $f;

//aplica lo mismo para el módulo
$e %= $f;

?>